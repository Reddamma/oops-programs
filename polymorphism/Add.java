package polymorphism;

public class Add {
	  public static void main(String args[])
	  {
		  PolyAdd a=new PolyAdd();

	    System.out.println(a.add(2,3));

	    System.out.println(a.add(2,3,4));

	    System.out.println(a.add(2,3.4));
	  }

	}

